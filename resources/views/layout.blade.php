<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 04/02/2019
 * Time: 11:20
 */
?>
<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Logo</title>
</head>
<body>

</body>
</html>
<header>
    <div class="navbar-laravel col-12">

        <img src="{{asset('svg/logo.png')}}" alt="">

        <nav>
            <ul>
                <li><a href="/home">Orders</a></li>
                <li><a href="/gestor">Providers</a></li>
                <li><a href="/settings">Products</a></li>
                <li><a href="/login">Log In</a></li>
                <li><a href="/signup">Sign Up</a></li>
            </ul>
            <i class="fa fa-bars"></i>
        </nav>

    </div>
</header>

@yield('content')
