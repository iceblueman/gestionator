<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 04/02/2019
 * Time: 11:24
 */
?>

@extends('layout')
@section('content')
    <div class="orders-container col-lg-12">
        <table class="orders-table col-lg-8 col-sm-12">
            <tr><th colspan="4">ORDERS <button class="btn btn-primary">New<i class="fa fa-plus-circle"></i></button></th></tr>
            <tr class="header">
                <td>ID</td>
                <td>NAME</td>
                <td>DATE</td>
                <td>STATUS</td>
            </tr>
            @for($i=0;$i<10;$i++)
                <tr>
                    <td>{{$i}}</td>
                    <td>Name</td>
                    <td>Today</td>
                    <td>Waiting</td>
                </tr>
            @endfor
        </table>
    </div>
@endsection


